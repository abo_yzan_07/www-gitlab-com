---
layout: markdown_page
title: "Version Control & Collaboration"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

<!------------------Begin page additions below this line ------------------ -->

## Version Control Overview


**Azure DevOps** offers two methods of version control as part of their SCM functionality for both the SaaS and on-premie versions:

* [Git](https://www.visualstudio.com/team-services/git/) (distributed) - each developer has a copy on their dev machine of the source repository including all branch and history information.

* [Team Foundation Version Control (TFVC)](https://www.visualstudio.com/team-services/tfvc/)), a centralized, client-server system - developers have only one version of each file on their dev machines. Historical data is maintained only on the server.

Microsoft recommends customers use Git for version control unless there is a specific need for centralized version control features: [See here](https://docs.microsoft.com/en-us/vsts/tfvc/comparison-git-tfvc).  This is noteworthy given that in June of 2018 Microsoft purchased [GitHub](../github/), the Internets largest online code repository.

**GitLab** does not provide support for Team Foundation Version Control, however, this is not a highly desired version control method.

How do we compare:

|                                    | GitLab | Azure DevOps |
|------------------------------------|--------|--------------|
| Unlimited private Git repo hosting |   Yes  |      Yes     |
| Diff in-line threaded code reviews |   Yes  |      Yes     |
|   Branch policies defining merge   |   Yes  |      Yes     |
|            Pull requests           |   Yes  |      Yes     |
|        Semantic code search        |   Yes  |      Yes     |
|       Webhooks and REST APIs       |   Yes  |      Yes     |

## Collaboration Overview

**Azure DevOps** provides persona-centric dashboards and views that resonate with customers, allowing them to reach not only the developers but the mindshare of executive leadership and therefore, sell higher in the enterprise.

**GitLab** has some built-in graphs and dashboards, but nothing as extensive as the Azure DevOps offering, and not targeted at higher roles in the enterprise.

How do we compare:

|                                                                        | GitLab | Azure DevOps |
|------------------------------------------------------------------------|--------|--------------|
|                           Work/issue tracking                          |   Yes  |      Yes     |
|                                Backlogs                                |   Yes  |      Yes     |
|                             Team dashboards                            |   No   |      Yes     |
|                            Custom reporting                            |   No   |      Yes     |
|                              Kanban boards                             |   Yes  |      Yes     |
|                    Scrum boards and sprint planning                    |   Yes  |      Yes     |
|                    Customizable work item workflows                    |   Yes  |      Yes     |
| Test & Feedback (exploratory/manual testing) - capture & record issues |   No   |      Yes     |
|                   Test planning, tracking & execution                  |   No   |      Yes     |
|                Load testing (Azure DevOps and VSTS only)               |   No   |      Yes     |
|                         User acceptance testing                        |   Yes  |      Yes     |
|                          Centralized reporting                         |   Yes  |      Yes     |
