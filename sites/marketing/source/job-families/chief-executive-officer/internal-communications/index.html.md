---
layout: job_family_page
title: "Internal Communications"
---

## levels

## Principal Internal Communications Manager

### Job Grade 

The Principal Internal Communications Manager is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

- Influence and collaborate with E-Group and members of the Marketing and People teams to plan and improve company-wide team member communications including Slack, issue comments, and other asynchronous communication, Handbook education, company calls, breakout calls, group conversations, AMAs, etc.
- Help drive CEO internal communications through supporting Group Conversations, Contribute and priority initiatives through crafting messaging and creating materials
- Help moderate internal communications to ensure GitLab values are upheld as well as compliance with our code of conduct
- Provide ‘internal PR’ support, including crisis communications
- Act as a communications consultant to team members to help develop campaigns for internal programs or initiatives (e.g. GitLab referral program)
- Curate and publish key internal news (e.g., “Here are the 3 things you need to know” digest) on a weekly basis
- Partner closely with the Talent Brand Manager to ensure integrity and cohesion between our external talent brand and internal team member experience and effectively articulate GitLab’s culture and value proposition
- Collaborate with key stakeholders to develop specific communication plans (e.g., work with People Business Partners and People Operations on Engagement Survey communication)
- Plan, communicate, and coordinate celebratory occasions, company communication initiatives, volunteer activities, and other team member engagement opportunities

### Requirements

- Minimum of 5 years of relevant communications experience
- Demonstrate the capability to drive initiatives, develop thoughtful communication strategies, and deliver results at a senior level. 
- Self motivated and works as a team of one.
- Demonstrated capability to deliver internal communications that are representative of company values and culture in a global or international setting
- Enthusiasm for leading internal communications in a unique, fully remote environment where email and face-to-face meetings are not the standard communication media
- Excellent narration and writing skills
- Ability to navigate cultural differences and build global but locally relevant solutions
- Experienced social and communication skills (verbal and written), across all levels with significant experience in executive level communications
- Excellent organizational skills, time management, and priority setting
- Deadline oriented, and able to work in a fast-paced environment with ever-changing priorities
- Ability to work as a Manager of one, as well as work collaboratively
- Proficiency in communication tools such as Slack, Zoom, and G-Suite is a must
- You share and can role model [our values](/handbook/values), and work in accordance with those values
- Ability to use GitLab

### Performance Indicators

- [Percent of sent Slack messages that are not DMs > 25%](/handbook/communication/#avoid-direct-messages)
- [Team member engagement survey](/handbook/people-group/engagement/) score
- [Team member retention](/handbook/people-group/people-group-metrics/#team-member-retention)
- Ad hoc feedback from team members on specific programs and initiatives

## Director, Internal Communications 

The Internal Communications Director will help advance our team member experience and communication channels to support GitLab’s culture and values. They will work cross-functionally to ensure extraordinary information exchange for all team members throughout the employee life cycle. They will proactively lead internal communications for issues that are sensitive or high priority and drive campaigns from ideation through to rollout. 

### Job Grade 

The Director, Internal Communications  is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

- Influence and collaborate with E-Group and members of the Marketing and People teams to plan and improve company-wide team member communications including Slack, issue comments, and other asynchronous communication, Handbook education, company calls, breakout calls, group conversations, AMAs, etc.
- Help drive CEO internal communications through supporting Group Conversations, Contribute and priority initiatives through crafting messaging and creating materials
- Help moderate internal communications to ensure GitLab values are upheld as well as compliance with our code of conduct
- Provide ‘internal PR’ support, including crisis communications
- Act as a communications consultant to team members to help develop campaigns for internal programs or initiatives (e.g. GitLab referral program)
- Curate and publish key internal news (e.g., “Here are the 3 things you need to know” digest) on a weekly basis
- Partner closely with the Talent Brand Manager to ensure integrity and cohesion between our external talent brand and internal team member experience and effectively articulate GitLab’s culture and value proposition
- Collaborate with key stakeholders to develop specific communication plans (e.g., work with People Business Partners and People Operations on Engagement Survey communication)
- Plan, communicate, and coordinate celebratory occasions, company communication initiatives, volunteer activities, and other team member engagement opportunities

### Requirements

- Minimum of 5 years of relevant communications experience
- Demonstrates the capability to drive initiatives, develop thoughtful communication strategies, and deliver results at a Director level. 
- Self motivated and works as a team of one.
- Demonstrated capability to deliver internal communications that are representative of company values and culture in a global or international setting
- Enthusiasm for leading internal communications in a unique, fully remote environment where email and face-to-face meetings are not the standard communication media
- Enthusiasm for leading internal communications in a unique, fully remote environment where email and face-to-face meetings are not the standard communication media
- Excellent narration and writing skills
- Ability to navigate cultural differences and build global but locally relevant solutions
- Experienced social and communication skills (verbal and written), across all levels with significant experience in executive level communications
- Excellent organizational skills, time management, and priority setting
- Deadline oriented, and able to work in a fast-paced environment with ever-changing priorities
- Ability to work as a Manager of one, as well as work collaboratively
- Proficiency in communication tools such as Slack, Zoom, and G-Suite is a must
- You share and can role model [our values](/handbook/values), and work in accordance with those values
- Ability to use GitLab

### Performance Indicators

- [Percent of sent Slack messages that are not DMs > 25%](/handbook/communication/#avoid-direct-messages)
- [Team member engagement survey](/handbook/people-group/engagement/) score
- [Team member retention](/handbook/people-group/people-group-metrics/#team-member-retention)
- Ad hoc feedback from team members on specific programs and initiatives

### Career Ladder

The next step in the Internal Communications job family is to move to a senior leader job family of which we do not yet have defined at GitLab. 

### Hiring Process

- Qualified candidates will be invited to schedule a 30 minute screening call with one of our recruiters
- Next, candidates will be invited to schedule a 30 minute interview with our Chief People Officer
- After that, candidates will be invited to schedule a 45 minute interview with members of the People and Marketing teams
- After that, candidates will be invited to interview with the Chief Marketing Officer and other business leaders
- Finally, our CEO may choose to conduct a final interview
