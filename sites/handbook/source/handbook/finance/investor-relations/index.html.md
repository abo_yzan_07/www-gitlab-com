--- 
layout: handbook-page-toc
title: "Investor Relations"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Earnings Release Calendar (workback schedule)

Task/Item - Key Contributors

### 5 Weeks Prior - Kick Off & Debrief	
* Flash report to Audit Committee - PAO
* First check-in: debrief and develop key themes for stockholder letter - IR/CEO/CFO/CLO

### 4 Weeks Prior	
* Press release: date of earnings release, access instructions - IR
* Overview of prelim financials and key metrics, CFO topics	- IR/CFO/PAO/VP FP&A
* Audit process begins, preliminary financials available - PAO
* Business update & overview of department initiatives (1:1s) - IR/CMO/CRO/EVP Product

### 3 Weeks Prior	
* Stockholder letter, v1 - IR/CEO/CFO/CLO
* Opening remarks, v1	- IR/CEO/CFO/CLO
* Q&A document, v1 - IR/PAO/VP FP&A

### 2 Weeks Prior	
* Stockholder letter, v2 - IR/CEO/CFO/CLO
* Opening remarks, v2 - IR/CEO/CFO/CLO
* Q&A document, v2 - IR/CFO/PAO
* Earnings press release, v1 - IR/CFO/CLO
* Update investor presentation - IR/Accounting/FP&A
* Submit documents for external legal review - CLO

### 1 Week Prior	
* Stockholder letter, vF - IR/CEO/CFO/CLO/VP FP&A
* Opening remarks, vF - IR/CEO/CFO/CLO
* Q&A document, vF and simulated Q&A first session - IR/CEO/CFO/CLO
* Audit Committee approval: earnings release, 10-Q/K - PAO/Audit Committee
* Disclosure Committee review all docs and address open comments/issues - [Disclosure Committee](/handbook/internal-audit/sarbanes-oxley/#disclosure-committee-charter)

### 3 Days Prior	
* Lock documents: stockholder letter, opening remarks, and earnings release	
* Share stockholder letter with Audit Committee and CMO  
* Read-through opening remarks, simulated Q&A second session - IR/CEO/CFO/CLO/VP FP&A

### 1 Day Prior	
* Final rehearsal of talking points, simulated Q&A third session - IR/CEO/CFO/CLO/VP FP&A

### Day of Earnings	
* Final Q&A Prep - IR/CEO/CFO/CLO/VP FP&A
* Earning release proof	- Finance/Accounting
* Shareholder Letter (tables/talking points) - IR/CEO/CLO/Finance
* NYSE / Nasdaq notification - IR
* Release hits wire	- IR
* 8-k filed with SEC - CLO
* Host earnings call - IR/CEO/CFO
* Sell-side analyst callbacks	- IR/CEO/CFO/CLO/VP FP&A

### Day after Earnings	
* Team member company update - GitLab/Internal comms
* File 10-Q
* Buy-side callbacks - IR/CEO/CFO

##### Continuous updates: abridged version of financials and key operating metrics, consensus, and Q&A tracker 

## Trading Window

We anticipate that our quarterly trading window will open the third trading day after the announcement of our quarterly results and that it will close again immediately prior to the last four weeks of the fiscal quarter, as indicated by an announcement made by the CLO. However, it is important to note that any quarterly trading window may be cut short if the CLO determines that material nonpublic information exists in such a fashion that it would make trades by directors, employees, and consultants inappropriate.

## Monthly investor update email

We send out an investor email. It consists of seven parts:

1. CEO foreword: A brief introduction that will typically coincide with the close of a fiscal quarter. This narrative will provide a high level overview of company operations from the most recently ended quarter as well as key initiatives and expectations for upcoming quarters.
1. Thanks: We express gratitude for investors who have assisted us with making introductions, providing feedback, or offering assistance. Investing is a type of social engagement, and we like to celebrate people who set aside time to help us.
1. Asks: We ask our investors to help us connect with people or organizations, introduce us to hiring candidates, or provide some other assistance. Investors can be extremely helpful and often say they want to add value when they invest, so this area of the update gives them the opportunity to drive our business forward.
1. Key metrics: People want to know how their investment is performing. Offering figures instills trust and shows a certain discipline and rigor. We want our investors to know how we’re doing - even when we don’t meet our goals - because we believe in transparency.
1. Lowlights: Our commitment to open communication extends to this section in which we always list the top three worst things that occurred in the month. By committing to three items, the question is no longer, “Should I tell my investors?” It’s “Which three things are the most severe?” That's a much easier question to answer.
1. Highlights: This section gets people excited about the investment and illustrates what we’re doing well. Every month we send three highlights.
1. Expectations: We discuss what we’re looking forward to, conferences we’re attending, and what we’re planning in the next month.

The email includes a link to the [GitLab Metrics](/handbook/business-ops/data-team/kpi-index/#gitlab-metrics) sheet.

We distribute the investor update by the 10th day after the end of the month. 

The draft email is created in a google doc that is shared in #investor-update on slack. The google doc should not have any html as the document is pasted into an email for distribution by the CEO. 

We first laid this out in [a blog post which we no longer keep up to date](/blog/2018/10/17/how-we-keep-investors-in-the-loop/).

## Performance Indicators (assuming publicly traded)

On a rolling 12-month basis, be among the top quartile of least volatile stocks compared to GitLab's public company peer set consisting of DevOps and software growth firms.

Greater than two-thirds of active, covering sell-side analysts describe the Company the same way we do: conclusions will vary.

Greater than 80% of questions during an earnings call have been anticipated.

### Enterprise Value to Sales
Enterprise Value to Sales compares the enterprise value (EV) of a company to its annual sales

Enterprise Value to Sales = Enterprise Value/Annual Sales

Enterprise Value = Market Capitalization + Debt - Cash and Cash Equivalents
