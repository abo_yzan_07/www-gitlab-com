require 'spec_helper'
require_relative '../../extensions/codeowners'

describe Codeowners do
  subject(:extension) { described_class.new(app_mock, options) }
  let(:app_mock) { double('App', after_configuration: nil, data: double(team: [{}])) }
  let(:options) { {} }

  it 'is registered as an extension' do
    expect(Middleman::Extensions.registered.key?(:codeowners)).to be true
  end

  describe '#codeowners_for_page' do
    subject { extension.codeowners_for_page(page) }
    before { allow(extension).to receive(:root) { '/root' } }
    let(:page) { double('Page', source_file: '/root/sites/handbook/source/handbook/ceo/index.html') }

    it { is_expected.to eq([{ description: '@sytses', url: 'https://gitlab.com/sytses', gitlab_name: '@sytses', image_url: nil }, { description: '@cheriholmes', url: 'https://gitlab.com/cheriholmes', gitlab_name: '@cheriholmes', image_url: nil }]) }
  end

  describe 'UserPresenter' do
    subject(:presenter) { Codeowners::UserPresenter.new(team) }

    let(:team) { [{ 'gitlab' => 'john', 'name' => 'John Smith', 'picture' => 'john.png' }] }

    describe '#present' do
      subject { presenter.present(username) }

      context 'when username is a real gitlab name' do
        let(:username) { '@john' }

        it { is_expected.to eq(description: 'John Smith', url: 'https://gitlab.com/john', gitlab_name: '@john', image_url: '/images/team/john-crop.jpg') }
      end

      context 'when username is an unknown gitlab name' do
        let(:username) { '@sam' }

        it { is_expected.to eq(description: '@sam', url: 'https://gitlab.com/sam', gitlab_name: '@sam', image_url: nil) }
      end

      context 'when username is an email' do
        let(:username) { 'john@gitlab.com' }

        it { is_expected.to eq(description: 'john@gitlab.com', url: nil, gitlab_name: nil, image_url: nil) }
      end

      context 'when user has a placeholder image' do
        let(:team) { [{ 'gitlab' => 'john', 'name' => 'John Smith', 'picture' => '../gitlab-logo-extra-whitespace.png' }] }
        let(:username) { '@john' }

        it { is_expected.to eq(description: 'John Smith', url: 'https://gitlab.com/john', gitlab_name: '@john', image_url: '/images/team/../gitlab-logo-extra-whitespace.png') }
      end
    end
  end
end
